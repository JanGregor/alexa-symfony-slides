var gulp = require('gulp');

var pug = require('gulp-pug');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var copy = require('gulp-copy');


gulp.task('html', function buildHTML() {
    return gulp.src('./views/*.pug')
        .pipe(pug({}))
        .pipe(gulp.dest('./build'));
})

gulp.task('js', function builJavaScript() {
    var files = [
        './node_modules/reveal.js/js/reveal.js',
        './node_modules/reveal.js/plugin/highlight/highlight.js',
        './src/main.js'
    ];

    return gulp.src(files)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build'));
})

gulp.task('css', function () {
    var files = [
        './node_modules/reveal.js/css/reveal.css',
        './node_modules/reveal.js/css/theme/solarized.css',
        './node_modules/highlight.js/styles/solarized-light.css',
        './layout/main.scss'
    ];

    return gulp.src(files)
        .pipe(concat('main.scss'))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./build'));
});

gulp.task('svg', function () {
    var files = [
        './assets/*.svg'
    ];

    return gulp
        .src(files)
        .pipe(copy('./build'));
});

gulp.task('images', function () {
    var files = [
        './assets/*.{png,jpg,gif}'
    ];

    return gulp
        .src(files)
        .pipe(copy('./build'));
});

gulp.task('font', function () {
    var files = [
        './node_modules/reveal.js/lib/**/*',
        './node_modules/font-awesome/fonts/*'
    ];

    return gulp
        .src(files)
        .pipe(copy('./build', {
            prefix: 2
        }));
});

gulp.task('default', ['html', 'js', 'css', 'images', 'svg', 'font']);
