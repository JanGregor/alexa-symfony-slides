hljs.initHighlightingOnLoad();
Reveal.initialize({
    margin: 0.02,
    minScale: 0.1,
    maxScale: 2.0
});

// Making multidimensional slides work with onedimensional presenter
// Courtesy of: https://stackoverflow.com/a/23796291/1143315
Reveal.configure({
    keyboard: {
        37: Reveal.prev, // left
        38: Reveal.prev, // up
        39: Reveal.next, // right
        40: Reveal.next, // down
    }
});
